#ifndef CAR_H
#define CAR_H
#include <string>
#include <sstream>

using std::string;
using std::stringstream;

class Car
{
    public:
        Car();
        virtual ~Car();
        Car(const Car& other);
        Car& clone();

    protected:

    private:
        enum eColorV{V_YELLOW,V_BLUE,V_GREEN,V_RED,V_WHITE};
};

#endif // CAR_H
