#ifndef HAPPYSTATE_H
#define HAPPYSTATE_H
#include "Passenger.h"
#include "Mood.h"

class Passenger;

class HappyState : public Mood {
public:
    // Base
	HappyState();
	virtual ~HappyState();
	HappyState(const HappyState& other);
    HappyState& operator=(const HappyState& other);

    // State
	virtual void SetNormal(Passenger * p);
	virtual void SetAngry(Passenger * p);
};

#endif // HAPPYSTATE_H
