#ifndef BUS_H
#define BUS_H

#include "Passenger.h"
#include <string>
#include <sstream>
#include <vector>

using std::string;
using std::vector;
using std::cout;

/************
**** BUS ****
*************/

/**
This class contains all the logic of the bus.
(about passengers for example)
**/

class Bus
{
    private:
        Passenger* passenger; //AIP
        int passengerCount;

    public:
        /**
        CANONICAL FORM
        **/
        Bus();
        virtual ~Bus();
        Bus(const Bus& other);
        Bus& operator=(const Bus& other);

        string str()const;
        int getPassengerCount()const;

        void takePassengers(int nbPassengers);
        Passenger* getPassenger()const;

        void setPassengerAngry();
        bool isPassengerAngry()const;
        void setPassengerNormal();
        bool isPassengerNormal()const;
        void setPassengerHappy();
        bool isPassengerHappy()const;

};

#endif // BUS_H
