#ifndef ANGRYSTATE_H
#define ANGRYSTATE_H
#include "Passenger.h"
#include "Mood.h"

class Passenger;

class AngryState : public Mood {
public:
    // Base
	AngryState();
	virtual ~AngryState();
    AngryState(const AngryState& other);
    AngryState& operator=(const AngryState& other);

    // State
	virtual void SetHappy(Passenger * p);
	virtual void SetNormal(Passenger * p);
};

#endif // ANGRYSTATE_H
