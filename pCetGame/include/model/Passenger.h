#ifndef PASSENGER_H
#define PASSENGER_H
#include<string>
#include <iostream>
#include <string>
#include <sstream>


using std::string;
using std::cout;
using std::endl;
using std::stringstream;

class Mood;

class Passenger
{
    public:
    // Base
        Passenger(string firstName="Anonymous",string lastName="Anonymous");
        virtual ~Passenger();
        Passenger(const Passenger& other);
        Passenger& operator=(const Passenger& other);
        Passenger* clone();
    // Methods
        string str() const;

    // State
        enum State
        {
            ST_HAPPY,
            ST_NORMAL,
            ST_ANGRY
        };

        void SetHappy();
        void SetNormal();
        void SetAngry();

        bool isAngry()const;
        bool isNormal()const;
        bool isHappy()const;

        void SetState(State state);


    private:
        string firstName;
        string lastName;
        Mood * pMood;
};

#endif // PASSENGER_H
