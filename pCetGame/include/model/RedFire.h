#ifndef REDFIRE_H
#define REDFIRE_H

#include <string>
#include <sstream>


using std::string;
using std::stringstream;

/*
type : class
package : Model
this class mean the logic reaction of the redfire
*/

class RedFire
{
    public:
        RedFire();
        virtual ~RedFire();
        RedFire(const RedFire& other);

        void swapColor();
        bool getIsGreen()const;
        string str()const;

    private:
        bool isGreen;



};

#endif // REDFIRE_H
