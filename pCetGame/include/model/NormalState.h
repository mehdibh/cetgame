#ifndef NORMALSTATE_H
#define NORMALSTATE_H
#include "Passenger.h"
#include "Mood.h"

class Passenger;

class NormalState : public Mood {
public:
    // Base
	NormalState();
	virtual ~NormalState();
	NormalState(const NormalState& other);
    NormalState& operator=(const NormalState& other);

    // State
	virtual void SetHappy(Passenger * p);
	virtual void SetAngry(Passenger * p);
};

#endif // NORMALSTATE_H
