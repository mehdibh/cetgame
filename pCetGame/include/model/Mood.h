#ifndef MOOD_H
#define MOOD_H

#include "Passenger.h"

#include <string>
#include <iostream>

using std::string;
using std::cout;

class Mood
{
    public:
        // Base
        Mood(string name);
        virtual ~Mood();
        Mood(const Mood& other);
        Mood& operator=(const Mood& other);

        // State
        virtual void SetHappy(Passenger * p);
        virtual void SetNormal(Passenger * p);
        virtual void SetAngry(Passenger * p);

        string GetName(){return name;}

    private:
        string name;

};

#endif // MOOD_H
