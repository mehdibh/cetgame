#ifndef BUSSTOP_H
#define BUSSTOP_H

#include <vector>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <string>

using std::vector;
using std::string;
using std::stringstream;

#include <iostream>
using std::cout;
using std::endl;

/**
BUS STOP
This class implements all the methods to define the logical part of the bus stop.
It contains a random number of passengers who will go in the bus eventually.
**/
class BusStop
{

    private:
        int nbWaitingPassengers;
        int generateRandomNumber()const;

    public:
        BusStop();
        virtual ~BusStop();
        BusStop(const BusStop& other);
        BusStop& operator=(const BusStop& other);
        string str() const;

        int getNbWaitingPassengers() const;
        void emptyPassengersWaiting();

        void refillPassengers();

};

#endif // BUSSTOP_H
