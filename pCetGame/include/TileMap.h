#ifndef TILEMAP_H
#define TILEMAP_H
#include <SFML/Graphics.hpp>
#include <string>

using namespace sf;
using std::string;

/*** Type : Class
**** Used for : View
**** This class is used to create the map of the game using a tileset made by squares of 32x32px
**** without this class, the background of the game would just not exist :)
***/

class TileMap : public Drawable, public Transformable
{
    private:
        virtual void draw(RenderTarget& target, RenderStates states) const;
        VertexArray m_vertices;
        Texture m_tileset;

    public:
        TileMap();
        virtual ~TileMap();
        TileMap(const TileMap& other);
        TileMap& operator=(const TileMap& other);

        bool load(const string& tileset, Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height);

    protected:


};

#endif // TILEMAP_H

