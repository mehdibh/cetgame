#ifndef TEXTPANEL_H
#define TEXTPANEL_H
#include <SFML/Graphics.hpp>
#include <sstream>
#include <string>
#include <IGraphicValues.h>

using std::stringstream;
using std::string;

using sf::Font;
using sf::Text;
using sf::Color;

class TextPanel
{
    public:
        TextPanel();
        virtual ~TextPanel();
        TextPanel(const TextPanel& other);
        TextPanel& operator=(const TextPanel& other);

        void followView(int speed);

        Text getScoreText()const;
        void setScore(int points);
        void increaseScore(int points);
        void decreaseScore(int points);
        void printScore(int scoreFinal);

        Text getNbPassagersInBusText()const;
        void setNbPassagersInBus(int nb);
        void printNbPassagersInBus(int nb);

        Text getProchainArretText()const;
        void setProchainArret(int nb);
        void printProchainArret(int nb);

        Text getLifeScoreText()const;
        void setLifeScore(int life);
        void printLifeScore(int life);

        Text getMoodText()const;
        void setMood(string newMood);
        void printMood(string newMood);

        void initPosition();

    protected:

    private:
        Font font;

        Text scoreText;
        Text nbPassagersInBusText;
        Text prochainArretText;
        Text lifeScoreText;
        Text moodText;

        int score;
        int nbPassagersInBus;
        int prochainArret;
        int lifeScore;
        string mood;
};

#endif // TEXTPANEL_H
