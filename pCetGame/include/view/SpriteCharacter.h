#ifndef SPRITECHARACTER_H
#define SPRITECHARACTER_H
#include <SFML/Graphics.hpp>
#include <string>
#include <sstream>

#include <cstdlib>
#include <ctime>

#include <iostream>

using sf::Texture;
using sf::Sprite;
using sf::IntRect;
using std::string;
using std::stringstream;


class SpriteCharacter
{
    public:
        SpriteCharacter();

        virtual ~SpriteCharacter();
        SpriteCharacter(const SpriteCharacter& other);
        SpriteCharacter& operator=(const SpriteCharacter& other);

        Sprite getSprite()const;

        void walk(float speed);
        void animateWalking();

        SpriteCharacter* clone();

    protected:

    private:
        Texture texture;
        Sprite sprite;
        IntRect viewRectangle;

        void initViewRectangle();
        void initTexture();
        void initPosition();


};

#endif // SPRITECHARACTER_H
