#ifndef SPRITEBUSSTOP_H
#define SPRITEBUSSTOP_H

#include "BusStop.h"
#include <SFML/Graphics.hpp>

#include <iostream>
#include <cstdlib>
#include <ctime>
using std::cout;
using std::endl;

using sf::Sprite;
using sf::Texture;

/**
SPRITE BUS STOP
This class implements all the graphical part of the bus stop.
**/
class SpriteBusStop
{
    public:
        SpriteBusStop(int x = 500);
        virtual ~SpriteBusStop();

        int getNbPassengers() const;
        Sprite getSprite() const;
        bool isDisplayed(int busX);

        void resetNbWaitingPassengers() const;
        void refillPassengersAndResetPosition(int baseX);

        void setRollback(int x);

    private:
        BusStop* busStop; //AIP
        Sprite sprite;
        Texture texture;

        int generateBonus(int baseX);


};

#endif // SPRITEBUSSTOP_H
