#ifndef SPRITEREDFIRE_H
#define SPRITEREDFIRE_H
#include "RedFire.h"
#include <SFML/Graphics.hpp>

using namespace sf;

/*
type : class
package : View
this class is the visual of the logic class redfire
*/

class SpriteRedFire
{
    public:
        SpriteRedFire();
        virtual ~SpriteRedFire();
        SpriteRedFire(const SpriteRedFire& other);
        SpriteRedFire& operator=(const SpriteRedFire& other);

        void movePosition();

        Sprite getSprite();
        Texture getTexture();
        RedFire* getRedfire();

        void swapColor();

        void initPosition();

    protected:

    private:
        RedFire* redfire;
        Texture texture;
        Sprite sprite;

};

#endif // SPRITEREDFIRE_H
