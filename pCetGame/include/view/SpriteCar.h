#ifndef SPRITECAR_H
#define SPRITECAR_H
#include "Car.h"
#include "RedFire.h"
#include <cstdlib>
#include <ctime>
#include <SFML/Graphics.hpp>
#include "SpriteRedFire.h"
#include "SpriteBus.h"

using namespace sf;
/*
type : class
package : View
this class it's the visual of a car in the game
*/

class SpriteCar
{
    public:
        SpriteCar(int busPosition);
        virtual ~SpriteCar();
        SpriteCar(const SpriteCar& other);
        SpriteCar& operator=(const SpriteCar& other);

        Texture getTexture()const;
        Sprite getSprite()const;

        void InitPosition();
        void startCours(float speed);
        void setPosition();
        bool canMove(SpriteRedFire& redFire,SpriteBus& bus);

        bool destroyVehicule(int busPositionX,int busPositionY, int *code);

        void setRollBackCar(int xBack);

    private:
        Texture texture;
        Sprite sprite;

        int PositionY;
        int PositionX;

};

#endif // SPRITECAR_H
