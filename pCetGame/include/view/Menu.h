#ifndef MENU_H
#define MENU_H
#include <SFML/Graphics.hpp>

using sf::Texture;
using sf::Sprite;
using sf::Window;
using sf::Mouse;
using sf::Vector2i;

/*** Type : Class
**** Used for : View
**** This class is used to show a Menu at the begining of the game
**** The menu is made of 2 Buttons : "Jouer" and "Quitter"
***/

class Menu
{
    public:
        Menu();
        virtual ~Menu();
        Menu(const Menu& other);
        Menu& operator=(const Menu& other);

        Sprite getSpriteJouer()const;
        Sprite getSpriteJouerHover()const;

        Sprite getSpriteQuitter()const;
        Sprite getSpriteQuitterHover()const;

        Sprite getBackgroundMenu()const;

        Sprite getSpriteToPrintJouer(Vector2i positions);
        Sprite getSpriteToPrintQuitter(Vector2i positions);

    protected:

    private:
        Texture textJouer;
        Texture textJouerHover;
        Texture textQuitter;
        Texture textQuitterHover;

        Texture textureBackgroundMenu;


        Sprite spriteJouer;
        Sprite spriteJouerHover;
        Sprite spriteQuitter;
        Sprite spriteQuitterHover;

        Sprite spriteBackgroundMenu;
};

#endif // MENU_H
