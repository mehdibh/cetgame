#ifndef GAMEOVERPANEL_H
#define GAMEOVERPANEL_H
#include <SFML/Graphics.hpp>

using sf::Texture;
using sf::Sprite;
using sf::Window;
using sf::Mouse;
using sf::Vector2i;


/*** Type : Class
**** Used for : View
**** This class is used to show a Game Over screen at the end of the game
***/

class GameOverPanel
{
    public:
        GameOverPanel();
        virtual ~GameOverPanel();
        GameOverPanel(const GameOverPanel& other);
        GameOverPanel& operator=(const GameOverPanel& other);

        Sprite getSpriteBackground()const;
        Sprite getSpriteToPrintQuitter(Vector2i positions);

    protected:

    private:
        Texture textureBackground;
        Texture textQuitterHover;
        Texture textQuitter;

        Sprite spriteBackground;
        Sprite spriteQuitter;
        Sprite spriteQuitterHover;
};

#endif // GAMEOVERPANEL_H
