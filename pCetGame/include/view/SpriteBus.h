#ifndef SPRITEBUS_H
#define SPRITEBUS_H
#include <SFML/Graphics.hpp>
#include "Bus.h"
#include <string>
#include <sstream>



#include <iostream>
using std::cout;
using std::endl;

using sf::Texture;
using sf::Sprite;
using std::string;
using std::stringstream;

/**
SPRITEBUS
This implements the graphical form of the bus.
All the changes that involves the bus in the view is here.
**/
class SpriteBus
{
    public:
        /**
        CANONICAL FORM
        **/
        SpriteBus();
        virtual ~SpriteBus();
        SpriteBus(const SpriteBus& other);
        SpriteBus& operator=(const SpriteBus& other);

        string str() const;
        Sprite getSprite()const;

        void initPosition();
        void movePosition(bool isGoingUp);
        void followView(int speed);
        int stopAndFreeze(int currentSpeed, int default_value)const;
        bool canStop()const;

        bool isNextToBusStop(int busStopX)const;
        bool isNextToRedFire(int redfireX) const;
        void takePassengers(int nbPassengers)const;
        int getNbPassengers()const;
        void resetPosition();

        void resetPositionX();

        void setAngry()const;
        bool isAngry()const;
        void setNormal()const;
        bool isNormal()const;
        void setHappy()const;
        bool isHappy()const;

    protected:

    private:
        Sprite spriteBus;
        Texture texture;
        Bus* busModel; //AIP: the bus that contains all the methods for the logic part.
};

#endif // SPRITEBUS_H
