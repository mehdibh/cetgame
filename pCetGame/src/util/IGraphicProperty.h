#ifndef IGRAPHICPROPERTY_H
#define IGRAPHICPROPERTY_H
#include <string>

using std::string;

class IGraphicProperty
{
    public:
        IGraphicProperty();

        int getWindowHeight()const;
        int getWindowWidth()const;
        int getCameraSpeed() const;
        void setCameraSpeed(int i);
        int getDefaultCameraSpeed() const;
        string getGameName() const;

        void resetCameraSpeed();

    private:
        int cameraSpeed;

};

#endif // IGRAPHICPROPERTY_H
