#include "Bus.h"

using std::stringstream;

Bus::Bus()
{
    //ctor
    passengerCount = 0;
    passenger = new Passenger();
}

Bus::~Bus()
{
    //dtor
    delete passenger;
}

Bus::Bus(const Bus& other)
{
    //copy ctor
    passenger = other.passenger;
    passengerCount = other.passengerCount;
}

Bus& Bus::operator=(const Bus& other) {
    if(this == &other) return *this;
    return *this;
}

string Bus::str()const {
    //TO STRING of Bus
    stringstream ssStr;
    ssStr << "Bus contains " << passengerCount << " passengers";
    return ssStr.str();
}

int Bus::getPassengerCount()const {
    return passengerCount;
}

void Bus::takePassengers(int nbPassengers) {
    passengerCount += nbPassengers;
}

Passenger* Bus::getPassenger()const {
    return passenger;
}

void Bus::setPassengerAngry() {
    passenger->SetAngry();
}

bool Bus::isPassengerAngry()const {
    return passenger->isAngry();
}

void Bus::setPassengerNormal() {
    passenger->SetNormal();
}

bool Bus::isPassengerNormal()const {
    return passenger->isNormal();
}

void Bus::setPassengerHappy() {
    passenger->SetHappy();
}

bool Bus::isPassengerHappy()const {
    return passenger->isHappy();
}
