#include "NormalState.h"

/*************************************** BASE ******************************************/

NormalState::NormalState(): Mood(string("Normal")){/* Constructor */}

NormalState::~NormalState(){/* Destructor */}

NormalState::NormalState(const NormalState& other) : Mood(string("Normal")){/* Copy Constructor */}

NormalState& NormalState::operator=(const NormalState& other){
    if (this == &other) return *this; // handle self assignment
    //assignment operator
    return *this;
}


/*************************************** STATE ******************************************/

void NormalState::SetHappy(Passenger * p)
{
	p->SetState(Passenger::ST_HAPPY);
}

void NormalState::SetAngry(Passenger * p)
{
	p->SetState(Passenger::ST_ANGRY);
}
