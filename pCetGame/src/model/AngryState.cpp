#include "AngryState.h"

/*************************************** BASE ******************************************/

AngryState::AngryState(): Mood(string("Angry")) {/*Constructor*/}

AngryState::~AngryState() {/*Destructor*/}

AngryState::AngryState(const AngryState& other) : Mood(string("Angry")){/*Copy Constructor*/}

AngryState& AngryState::operator=(const AngryState& other){
    if (this == &other) return *this; // handle self assignment
    //assignment operator
    return *this;
}

/*************************************** STATE ******************************************/

void AngryState::SetHappy(Passenger * p)
{
	p->SetState(Passenger::ST_HAPPY);
}

void AngryState::SetNormal(Passenger * p)
{
	p->SetState(Passenger::ST_NORMAL);
}
