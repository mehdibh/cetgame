#include "Mood.h"

/*************************************** BASE ******************************************/

Mood::Mood(string name): name(name) {/* Constructor */}

Mood::~Mood() {/* Destructor */}

Mood::Mood(const Mood& other): name(other.name) {/* Copy Constructor */}

Mood& Mood::operator=(const Mood& other){
    if(this != &other)
    {
        this->name = other.name;
    }
    return *this;
}


/*************************************** STATE ******************************************/
//These methods are implemented in the class which herits from this
void Mood::SetHappy(Passenger *p)
{
    //Set the mood to happy
}

void Mood::SetNormal(Passenger *p)
{
    //Set the mood to neutral
}

void Mood::SetAngry(Passenger *p)
{
    //Set the mood to angry
}
