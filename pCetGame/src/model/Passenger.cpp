#include "Passenger.h"

#include "HappyState.h"
#include "NormalState.h"
#include "AngryState.h"

/*************************************** BASE ******************************************/
Passenger::Passenger(string firstName,string lastName):pMood(new NormalState())
{
    // Constructor
    this->firstName = firstName;
    this->lastName = lastName;
}

Passenger::~Passenger()
{
    // Destructor
    delete pMood;
}

Passenger::Passenger(const Passenger& other)
{
    // Copy Constructor
    this->firstName = other.firstName;
    this->lastName = other.lastName;
    this->pMood = other.pMood;
}

Passenger& Passenger::operator=(const Passenger& other)
{
    if(&other!=this){
        this->firstName = other.firstName;
        this->lastName = other.lastName;
        this->pMood = other.pMood;
    }
    return *this;
}

Passenger* Passenger::clone() {
    return new Passenger(*this);
}

/*************************************** STATE ******************************************/
void Passenger::SetHappy() {
	pMood->SetHappy(this);
}
void Passenger::SetNormal() {
	pMood->SetNormal(this);
}
void Passenger::SetAngry() {
	pMood->SetAngry(this);
}

void Passenger::SetState(State state)
{
	delete pMood;

	if(state == ST_HAPPY)
	{
		pMood = new HappyState();
	}
	else if(state == ST_NORMAL)
	{
		pMood = new NormalState();
	}
	else
	{
		pMood = new AngryState();
	}
}

/*************************************** METHODS ******************************************/
string Passenger::str()const{
    stringstream sstr;
    sstr<<"Firstname : "<<firstName<<endl;
    sstr<<"Lastname : "<<lastName<<endl;
    sstr<<"Mood : "<<pMood->GetName()<<endl;
    return sstr.str();
}

bool Passenger::isAngry()const {
    bool isAngry = false;
    AngryState* angry = new AngryState();
    isAngry = (this->pMood->GetName() == angry->GetName());
    delete angry;
    return isAngry;
}

bool Passenger::isNormal()const {
    bool isNormal = false;
    NormalState* normal = new NormalState();
    isNormal = (this->pMood->GetName() == normal->GetName());
    delete normal;
    return isNormal;
}

bool Passenger::isHappy()const {
    bool isHappy = false;
    HappyState* happy = new HappyState();
    isHappy = (this->pMood->GetName() == happy->GetName());
    delete happy;
    return isHappy;
}
