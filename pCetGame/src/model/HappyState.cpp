#include "HappyState.h"

/*************************************** BASE ******************************************/

HappyState::HappyState(): Mood(string("Happy")){/* Constructor */}

HappyState::~HappyState(){/* Destructor */}

HappyState::HappyState(const HappyState& other) : Mood(string("Happy")){/* Copy Constructor */}

HappyState& HappyState::operator=(const HappyState& other){
    if (this == &other) return *this; // handle self assignment
    //assignment operator
    return *this;
}


/*************************************** STATE ******************************************/

void HappyState::SetNormal(Passenger * p)
{
	p->SetState(Passenger::ST_NORMAL);
}

void HappyState::SetAngry(Passenger * p)
{
	p->SetState(Passenger::ST_ANGRY);
}

