#include "BusStop.h"

BusStop::BusStop()
{
    //ctor
    refillPassengers();
}

BusStop::~BusStop()
{
    //dtor
}

BusStop::BusStop(const BusStop& other)
{
    //copy ctor
    this->nbWaitingPassengers = other.nbWaitingPassengers;
}

BusStop& BusStop::operator=(const BusStop& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    this->nbWaitingPassengers = rhs.nbWaitingPassengers;
    return *this;
}

string BusStop::str() const {
    stringstream ssStr;
    ssStr << nbWaitingPassengers << " people are waiting for the bus to stop.";
    return ssStr.str();
}

int BusStop::getNbWaitingPassengers() const {
    //Waiting passengers at the bus stop
    return nbWaitingPassengers;
}

void BusStop::emptyPassengersWaiting() {
    //Reset passengers number when they have all gone in the bus
    nbWaitingPassengers = 0;
}

void BusStop::refillPassengers() {
    //Put new passengers at the bus stop waiting for the bus
    nbWaitingPassengers = generateRandomNumber();
}

int BusStop::generateRandomNumber()const {
    //Generate random number
    srand(time(NULL));
    return (rand()%15) + 1;
}
