#include "RedFire.h"

RedFire::RedFire()
{
    //ctor
    this->isGreen = true;
}

RedFire::~RedFire()
{
    //dtor
}

RedFire::RedFire(const RedFire& other)
{
    //copy ctor
}
// return true if the redfire is green and swap this for the next call it is red
void RedFire::swapColor()
{
    if(isGreen){isGreen = false;return;}
    isGreen = true;return;
}

bool RedFire::getIsGreen()const{
    return isGreen;
}

string RedFire::str()const{
    stringstream strs;
    strs<<"the fire is green ? "<<this->isGreen;
    return strs.str();
}
