#include "TextPanel.h"

TextPanel::TextPanel()
{
    /*** SFML Method Call : Used to load a Font from a Font File ***/
    font.loadFromFile("fonts/PrancerDeer.ttf");

    /**************
    **** SCORE ****
    **************/
    setScore(0);

    /*** SFML Method Call : Used to set the font to a Text ***/
    scoreText.setFont(font);

    /*** SFML Method Call : Used to set the Pixel Size of the Text ***/
    scoreText.setCharacterSize(50);
    /*** SFML Method Call : Used to set the Style of the Text (Regular,Bold,Italic) ***/
    scoreText.setStyle(Text::Bold);
    /*** SFML Method Call : Used to set the color of the Text ***/
    scoreText.setFillColor(Color::Green);
    /*** SFML Method Call : Used to position the Text in the View ***/
    scoreText.setPosition(10,430);
    /*** Method Call : Used to display the score ***/
    printScore(score);


    /*****************************
    **** Number of passengers ****
    *****************************/
    nbPassagersInBusText.setFont(font);
    nbPassagersInBusText.setCharacterSize(50);
    nbPassagersInBusText.setFillColor(Color::Green);
    nbPassagersInBusText.setPosition(700,430);
    printNbPassagersInBus(0);


    /**************************************
    **** Distance to the next bus stop ****
    **************************************/
    prochainArretText.setFont(font);
    prochainArretText.setCharacterSize(50);
    prochainArretText.setFillColor(Color::Yellow);
    prochainArretText.setPosition(250,384);
    printProchainArret(150);


    /************************
    **** Number of lives ****
    ************************/
    lifeScoreText.setFont(font);
    lifeScoreText.setCharacterSize(50);
    lifeScoreText.setFillColor(Color::Red);
    lifeScoreText.setPosition(400,440);
    printLifeScore(lifeScore);

    /*************
    **** MOOD ****
    *************/
    moodText.setFont(font);
    moodText.setCharacterSize(30);
    moodText.setFillColor(Color::Blue);
    moodText.setPosition(755,390);
    printMood(mood);
}

TextPanel::~TextPanel(){}

TextPanel::TextPanel(const TextPanel& other){}

TextPanel& TextPanel::operator=(const TextPanel& rhs){if (this == &rhs) return *this;return *this;}


/*** Type : Method
**** Used for : View
**** This method is used to move the elements of the "Text Panel" in the view
**** it means that the camera will go at the same speed as the elements and so the player will think the TextPanel is not moving
***/
void TextPanel::followView(int speed){
   scoreText.move(speed,0);
   nbPassagersInBusText.move(speed,0);
   prochainArretText.move(speed,0);
   lifeScoreText.move(speed,0);
   moodText.move(speed,0);

}

/**************
**** SCORE ****
**************/

Text TextPanel::getScoreText()const{
    return scoreText;
}

void TextPanel::setScore(int points){
    score = points;
    printScore(score);
}

void TextPanel::increaseScore(int points){
    score += points;
    printScore(score);
}

void TextPanel::decreaseScore(int points){
    score -= points;
    if(score < 0) score = 0;
    printScore(score);
}

void TextPanel::printScore(int scoreFinal){
    stringstream scoreConcat;
    scoreConcat<<"Score : "<<scoreFinal;
    scoreText.setString(scoreConcat.str());
}

/*****************************
**** Number of passengers ****
*****************************/

Text TextPanel::getNbPassagersInBusText()const{
    return nbPassagersInBusText;
}

void TextPanel::setNbPassagersInBus(int nb){
    nbPassagersInBus = nb;
    printNbPassagersInBus(nbPassagersInBus);
}

void TextPanel::printNbPassagersInBus(int nb){
    stringstream nbPassagersInBusConcat;
    nbPassagersInBusConcat<<"Passagers : "<<nb;
    nbPassagersInBusText.setString(nbPassagersInBusConcat.str());
}

/**************************************
**** Distance to the next bus stop ****
**************************************/

Text TextPanel::getProchainArretText()const{
    return prochainArretText;
}

void TextPanel::setProchainArret(int nb){
    prochainArret = nb;
    printProchainArret(prochainArret);
}

void TextPanel::printProchainArret(int nb){
    stringstream prochainArretConcat;
    prochainArretConcat<<"Prochain arr�t : "<<nb<<" m";
    prochainArretText.setString(prochainArretConcat.str());
}

/************************
**** Number of lives ****
************************/

Text TextPanel::getLifeScoreText()const
{
    return lifeScoreText;
}

void TextPanel::setLifeScore(int life)
{
    lifeScore = life;
    printLifeScore(lifeScore);
}

void TextPanel::printLifeScore(int life)
{
    stringstream lifeScoreConcat;
    lifeScoreConcat<<"VIES : "<<life;
    lifeScoreText.setString(lifeScoreConcat.str());
}

/**************
**** MOOD ****
**************/
Text TextPanel::getMoodText()const
{
    return this->moodText;
}

void TextPanel::setMood(string newMood)
{
    mood = newMood;
    if(newMood == HAPPY_MOOD)
    {
        moodText.setFillColor(Color::Blue);
    }
    else if(newMood == NORMAL_MOOD)
    {
        moodText.setFillColor(Color::White);
    }
    else if(newMood == ANGRY_MOOD)
    {
        moodText.setFillColor(Color::Red);
    }
    printMood(mood);
}

void TextPanel::printMood(string mood)
{
    stringstream moodConcat;
    moodConcat<<mood;
    moodText.setString(moodConcat.str());
}

void TextPanel::initPosition()
{
    scoreText.setPosition(10,430);
    nbPassagersInBusText.setPosition(700,430);
    prochainArretText.setPosition(250,384);
    lifeScoreText.setPosition(400,440);
    moodText.setPosition(755,390);
}
