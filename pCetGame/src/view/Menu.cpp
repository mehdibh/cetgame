#include "Menu.h"

Menu::Menu()
{
    /*** SFML Method call : Used to load a texture from an image ***/
    textJouer.loadFromFile("images/buttons/jouer.png");
    textJouerHover.loadFromFile("images/buttons/jouer_hover.png");
    textQuitter.loadFromFile("images/buttons/quitter.png");
    textQuitterHover.loadFromFile("images/buttons/quitter_hover.png");

    textureBackgroundMenu.loadFromFile("images/menu.png");

    /*** SFML Method call : Used to set a texture to a Sprite ***/
    spriteJouer.setTexture(textJouer);
    spriteJouerHover.setTexture(textJouerHover);
    spriteQuitter.setTexture(textQuitter);
    spriteQuitterHover.setTexture(textQuitterHover);

    spriteBackgroundMenu.setTexture(textureBackgroundMenu);

    /*** SFML Method call : Used to position the "Sprites" in the View ***/
    spriteJouer.setPosition(2370,2050);
    spriteJouerHover.setPosition(2370,2050);
    spriteQuitter.setPosition(2370,2300);
    spriteQuitterHover.setPosition(2370,2300);

    spriteBackgroundMenu.setPosition(2000,2000);


}

Menu::~Menu(){}

Menu::Menu(const Menu& other){}

Menu& Menu::operator=(const Menu& rhs){if (this == &rhs) return *this;return *this;}

/******************************************************************************************/

/*** Type : Method
**** Used for : View
**** This method is used to return the "Normal Sprite" or the "Hover Sprite" for the button "JOUER" according to the mouse position
**** it means that : if the mouse is on the button, the "Hover Sprite" will be returned
***/
Sprite Menu::getSpriteToPrintJouer(Vector2i positions)
{
    int xMouse = positions.x;
    int yMouse = positions.y;
    if(xMouse>370 && xMouse<670 && yMouse >50 && yMouse<150)
    {
        return this->spriteJouerHover;

    }
    else
    {
        return this->spriteJouer;
    }
}

/*** Type : Method
**** Used for : View
**** This method is used to return the "Normal Sprite" or the "Hover Sprite" for the button "QUITTER" according to the mouse position
**** it means that : if the mouse is on the button, the "Hover Sprite" will be returned
***/
Sprite Menu::getSpriteToPrintQuitter(Vector2i positions)
{
    int xMouse = positions.x;
    int yMouse = positions.y;
    if(xMouse>370 && xMouse<670 && yMouse >300&& yMouse<400)
    {
        return this->spriteQuitter;
    }
    else
    {
        return this->spriteQuitterHover;
    }
}

Sprite Menu::getSpriteJouer()const
{
    return this->spriteJouer;
}

Sprite Menu::getSpriteJouerHover()const
{
    return this->spriteJouerHover;
}

Sprite Menu::getSpriteQuitter()const
{
    return this->spriteQuitter;
}

Sprite Menu::getSpriteQuitterHover()const
{
    return this->spriteQuitterHover;
}

Sprite Menu::getBackgroundMenu()const
{
    return this->spriteBackgroundMenu;
}
