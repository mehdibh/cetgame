#include "GameOverPanel.h"

GameOverPanel::GameOverPanel()
{
    /*** SFML Method call : Used to load a texture from an image ***/
    textureBackground.loadFromFile("images/gameover.png");
    textQuitterHover.loadFromFile("images/buttons/quitter_hover.png");
    textQuitter.loadFromFile("images/buttons/quitter.png");

    /*** SFML Method call : Used to set a texture to a Sprite ***/
    spriteBackground.setTexture(textureBackground);
    spriteQuitterHover.setTexture(textQuitterHover);
    spriteQuitter.setTexture(textQuitter);

    /*** SFML Method call : Used to position the "Sprites" in the View ***/
    spriteBackground.setPosition(3000,3000);
    spriteQuitterHover.setPosition(3370,3300);
    spriteQuitter.setPosition(3370,3300);
}

GameOverPanel::~GameOverPanel(){}

GameOverPanel::GameOverPanel(const GameOverPanel& other){}

GameOverPanel& GameOverPanel::operator=(const GameOverPanel& rhs){if (this == &rhs) return *this;return *this;}

/***************************************************************************************************************/

Sprite GameOverPanel::getSpriteBackground()const
{
    return this->spriteBackground;
}

/*** Type : Method
**** Used for : View
**** This method is used to return the "Normal Sprite" or the "Hover Sprite" according to the mouse position
**** it means that : if the mouse is on the button, the "Hover Sprite" will be returned
***/

Sprite GameOverPanel::getSpriteToPrintQuitter(Vector2i positions)
{
    int xMouse = positions.x;
    int yMouse = positions.y;
    if(xMouse>370 && xMouse<670 && yMouse >300&& yMouse<400)
    {
        return this->spriteQuitter;
    }
    else
    {
        return this->spriteQuitterHover;
    }
}
