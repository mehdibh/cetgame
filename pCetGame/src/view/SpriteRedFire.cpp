#include "SpriteRedFire.h"

SpriteRedFire::SpriteRedFire()
{
    //ctor
    // initiate the redfire into the constructor
    this->redfire = new RedFire();
    // load the texture of the redfire
    texture.loadFromFile("images/greenfire.png");
    sprite.setTexture(texture);
    // init the scale and the position of the redfire
    sprite.scale(sf::Vector2f(0.2f,0.2f));
    sprite.setPosition(sf::Vector2f(350,46));
}

SpriteRedFire::~SpriteRedFire()
{
    //dtor
    delete redfire;
}

SpriteRedFire::SpriteRedFire(const SpriteRedFire& other)
{
    //copy ctor
    this->redfire = new RedFire(*other.redfire);
}



SpriteRedFire& SpriteRedFire::operator=(const SpriteRedFire& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

Sprite SpriteRedFire::getSprite()
{
    return this->sprite;
}

RedFire* SpriteRedFire::getRedfire()
{
    return this->redfire;
}

Texture SpriteRedFire::getTexture()
{
    return texture;
}
// move the redfire sprite on the next cross
void SpriteRedFire::movePosition(){
    this->sprite.move(1600,0);
}
// call the method swapColor of the redfire class but change the texture of the redfire
void SpriteRedFire::swapColor(){
    if(redfire->getIsGreen()){
        texture.loadFromFile("images/redfire.png");
        sprite.setTexture(texture);
        redfire->swapColor();
        return;
    }
    texture.loadFromFile("images/greenfire.png");
    sprite.setTexture(texture);
    redfire->swapColor();
}

void SpriteRedFire::initPosition()
{
    this->sprite.setPosition(1600,46);
}
