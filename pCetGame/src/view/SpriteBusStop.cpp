#include "SpriteBusStop.h"

#include <string>
#include <iostream>
using std::string;
using std::cout;
using std::endl;

SpriteBusStop::SpriteBusStop(int x)
{
    //ctor
    int bonus = generateBonus(x);
    busStop = new BusStop();
    /*
    Create a new instance of bus stop to have all the logical part of the bus stop.
    */
    texture.loadFromFile("images/bus_stop.png");
    sprite.setTexture(texture);
    sprite.scale(sf::Vector2f(0.5f,0.5f));
    sprite.setPosition(x, 180);
}

int SpriteBusStop::generateBonus(int baseX) {
    //generate a random number to add at the base x to eventually set the bus stop far away from the bus
    if(baseX == 300) return 0;
    return (rand()%100) + 5500;
}

SpriteBusStop::~SpriteBusStop()
{
    //dtor
    delete busStop; //delete pointer
}

int SpriteBusStop::getNbPassengers() const {
    //Get the number of waiting passengers from the model
    return busStop->getNbWaitingPassengers();
}

Sprite SpriteBusStop::getSprite() const {
    return sprite;
}

bool SpriteBusStop::isDisplayed(int busX) {
    //Check if the bus stop is rendered on the window
    //After that, if the stop is not displayed, it replaces it futher
    if(sprite.getPosition().x < (busX-150)) {
        return false;
    }
    return true;
}

void SpriteBusStop::resetNbWaitingPassengers() const {
    //Empty the bus stop from the model
    busStop->emptyPassengersWaiting();
}

void SpriteBusStop::refillPassengersAndResetPosition(int baseX) {
    //When the bus stop is not displayed, it resets all attributes and places it further.
    busStop->refillPassengers();
    sprite.setPosition(baseX+generateBonus(baseX),180);
}

void SpriteBusStop::setRollback(int x) {
    sprite.move(-x, 0);
}
