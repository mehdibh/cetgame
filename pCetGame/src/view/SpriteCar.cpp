#include "SpriteCar.h"

SpriteCar::SpriteCar(int busPosition)
{
    //ctor
    PositionY = 145; //bus = 86 BASE = 145
    PositionX = busPosition + 1050;
    // load the texture of this car
    texture.loadFromFile("images/car2.png");
    sprite.setTexture(texture);
    sprite.scale(sf::Vector2f(0.3f,0.3f));

    setPosition();

}

SpriteCar::~SpriteCar()
{
    //dtor
}

SpriteCar::SpriteCar(const SpriteCar& other)
{
    //copy ctor
}

// set the car position in the top,middle or bottom way
void SpriteCar::setPosition(){
    srand(time(NULL));
    int random = rand() % 3 + 1;
    switch(random){
    case 1 :
        sprite.setPosition(sf::Vector2f(PositionX,PositionY));
        break;
    case 2 :
        sprite.setPosition(sf::Vector2f(PositionX,PositionY+64));
        break;
    case 3 :
        sprite.setPosition(sf::Vector2f(PositionX,PositionY+128));
        break;
    default:
        break;
    }
    sprite.setRotation(180);
}

Texture SpriteCar::getTexture()const{
    return this->texture;
}

Sprite SpriteCar::getSprite()const{
    return this->sprite;
}
// Initiate the position of the car in the tilemap.
void SpriteCar::InitPosition(){
    setPosition();
}
// return true if the car isn't on redfire or if it gonna on the bus back
bool SpriteCar::canMove(SpriteRedFire& redfire,SpriteBus& bus){
    if(this->sprite.getPosition().x == redfire.getSprite().getPosition().x+20){ return false;}
    if(this->sprite.getPosition().x == bus.getSprite().getPosition().x-20){ return false;}
    return true;
}

// set the speed of the car.
void SpriteCar::startCours(float speed){
    this->sprite.move(speed,0);
}

SpriteCar& SpriteCar::operator=(const SpriteCar& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}


//life count
bool SpriteCar::destroyVehicule(int busPositionX,int busPositionY, int* code){
    *code = 0;
    if(busPositionX - 100 > this->sprite.getPosition().x)
    {
        return true;
    }
    if(busPositionX + 1300 > this->sprite.getPosition().x && busPositionX + 1400 < this->sprite.getPosition().x)
    {
        return true;
    }
    switch(busPositionY){
    case 86 :
        if(this->sprite.getPosition().y == 145)
        {
            if(busPositionX + 255 > this->sprite.getPosition().x && busPositionX - 5 < this->sprite.getPosition().x){
                *code = 1;
                return true;
            }
        }
        break;
    case 150 :
        if(this->sprite.getPosition().y == (145 + 64))
        {
            if(busPositionX + 255 > this->sprite.getPosition().x && busPositionX - 5 < this->sprite.getPosition().x){
                *code = 1;
                return true;
            }
        }
        break;
    case 214 :
        if(this->sprite.getPosition().y == (145 + 64 + 64))
        {
            if(busPositionX + 255 > this->sprite.getPosition().x && busPositionX - 5 < this->sprite.getPosition().x){
                *code = 1;
                return true;
            }
        }
        break;
    default:
        break;
    }
    return false;

}

void SpriteCar::setRollBackCar(int xBack)
{
    this->sprite.move(-xBack,0);
}


