#include "SpriteBus.h"
#include "IGraphicValues.h"

SpriteBus::SpriteBus()
{
    texture.loadFromFile("images/bus.png");
    spriteBus.setTexture(texture);
    spriteBus.setScale(sf::Vector2f(0.7f, 0.7f));
    busModel = new Bus();
    //CREATION OF A BUS POINTER
    /*
    To have an complete "instance" of the bus : the graphic and the logical part.
    */
}

SpriteBus::~SpriteBus()
{
    //dtor
    delete busModel; //delete pointers
}

SpriteBus::SpriteBus(const SpriteBus& other)
{
    //copy ctor
    delete busModel;
    busModel = other.busModel; //Only save the model (because the view is created yet.
}

SpriteBus& SpriteBus::operator=(const SpriteBus& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    delete busModel;
    busModel = rhs.busModel;
    return *this;
}

Sprite SpriteBus::getSprite() const {
    //Return the graphical implementation of the bus
    return spriteBus;
}

void SpriteBus::initPosition() {
    //Set the bus in the correct position to start the game
    spriteBus.setPosition(80,150);
}

void SpriteBus::followView(int speed){
    //Allows the bus to move forward on the road
   spriteBus.move(speed,0);
}

void SpriteBus::movePosition(bool isGoingUp) {

    //Move the bus up and down and don't go out of bounds
    int x = spriteBus.getPosition().x;
    int y = spriteBus.getPosition().y;

    if(isGoingUp) {
        if(y > 128) {
            y-=64; // don't go to high
        }
    } else {
        if(y < 192) {
            y+=64; // don't go to low
        }
    }

    spriteBus.setPosition(x, y);
}

string SpriteBus::str() const {
    stringstream ssStr;
    ssStr << busModel->str();
    return ssStr.str();
}

int SpriteBus::stopAndFreeze(int currentSpeed, int default_value) const {
    //Stop the bus and freeze the camera.
    if( !this->canStop() && currentSpeed != 0) { return -1; }
    if(currentSpeed == 0) { return 1; }
    return 0;
}

bool SpriteBus::canStop() const {
    //Verify that the bus is on the last line and can stop near a bus stop
    if(spriteBus.getPosition().y == 214) {
        return true;
    }
    return false;
}

bool SpriteBus::isNextToBusStop(int busStopX)const {
    //Verify that x coordinate is near the bus stop coordinate
    const int MARGIN_STOP_WIDTH = 80;
    if(canStop()) {
        if(spriteBus.getPosition().x >= busStopX - MARGIN_STOP_WIDTH && spriteBus.getPosition().x <= busStopX + MARGIN_STOP_WIDTH) {
            return true;
        }
    }
    return false;
}

bool SpriteBus::isNextToRedFire(int redfireX) const {
    //Verify that x coordinate is near the red fire coordinate
    const int MARGIN_STOP_WIDTH = 150;
    int spreading = spriteBus.getPosition().x - redfireX;
    if(spreading < 0) spreading*=-1;
    if(spreading <= MARGIN_STOP_WIDTH) {
        return true;
    }
    return false;
}

void SpriteBus::takePassengers(int nbPassengers) const {
    //The passengers go in the bus
    busModel->takePassengers(nbPassengers);
}

int SpriteBus::getNbPassengers()const {
    return busModel->getPassengerCount();
}

void SpriteBus::resetPosition() {
    //Reset the position to is right place when the game restart
    initPosition();
}


void SpriteBus::resetPositionX() {
    //Set the bus in the correct position to start the game
    spriteBus.setPosition(80,spriteBus.getPosition().y);
}

void SpriteBus::setAngry()const {
    busModel->setPassengerAngry();
}

bool SpriteBus::isAngry()const {
    return busModel->isPassengerAngry();
}

void SpriteBus::setNormal()const {
    return busModel->setPassengerNormal();
}

bool SpriteBus::isNormal()const {
    return busModel->isPassengerNormal();
}

void SpriteBus::setHappy()const {
    busModel->setPassengerHappy();
}

bool SpriteBus::isHappy()const {
    return busModel->isPassengerHappy();
}
