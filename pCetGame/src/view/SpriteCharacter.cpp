#include "SpriteCharacter.h"

SpriteCharacter::SpriteCharacter()
{
    //ctor
    initViewRectangle();
    initTexture();
    sprite.setTexture(texture);
    sprite.setTextureRect(viewRectangle);
    initPosition();
}

void SpriteCharacter::initViewRectangle() {
    viewRectangle.top = 192;
    viewRectangle.left = 0;
    viewRectangle.height = 64;
    viewRectangle.width = 64;
}

void SpriteCharacter::initTexture() {
    //srand(time(NULL));
    int random = (rand()%6) + 1;
    stringstream texturePath;
    texturePath << "images/characters/0" << random << ".png";
    texture.loadFromFile(texturePath.str());
}

void SpriteCharacter::initPosition() {
    //srand(time(NULL));
    int random_x = (rand()%100) + 1;
    int random_y = (rand()%30) + 1;

    bool isTop = (rand()%100) > 50;
    if(!isTop) random_y += 300;

    sprite.setPosition(random_x, random_y);
}

SpriteCharacter* SpriteCharacter::clone(){
    return new SpriteCharacter(*this);
}

SpriteCharacter::~SpriteCharacter()
{
    //dtor
}

SpriteCharacter::SpriteCharacter(const SpriteCharacter& other)
{
    //copy ctor
    this->texture = other.texture;
    this->viewRectangle = other.viewRectangle;

    this->sprite.setTexture(this->texture);
    this->sprite.setTextureRect(this->viewRectangle);
    this->sprite.setPosition(other.sprite.getPosition());
}

SpriteCharacter& SpriteCharacter::operator=(const SpriteCharacter& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

Sprite SpriteCharacter::getSprite() const {
    return sprite;
}

void SpriteCharacter::walk(float speed) {
    speed *= 2;
    sprite.move(speed, 0);
}

void SpriteCharacter::animateWalking() {
    viewRectangle.left += viewRectangle.width;
    if(viewRectangle.left >= viewRectangle.width * 9) {
        viewRectangle.left = 0;
    }
    sprite.setTextureRect(viewRectangle);
}


