#include "IGraphicProperty.h"

IGraphicProperty::IGraphicProperty()
{
    //ctor
    cameraSpeed = this->getDefaultCameraSpeed();
}

int IGraphicProperty::getWindowHeight() const {
    return 500;
}

int IGraphicProperty::getWindowWidth() const {
    return 1024;
}

int IGraphicProperty::getCameraSpeed() const {
    return cameraSpeed;
}

void IGraphicProperty::setCameraSpeed(int i) {
    cameraSpeed = i;
}

int IGraphicProperty::getDefaultCameraSpeed() const {
    return 1;
}

string IGraphicProperty::getGameName() const {
    return "CET";
}

void IGraphicProperty::resetCameraSpeed() {
    setCameraSpeed(this->getDefaultCameraSpeed());
}
