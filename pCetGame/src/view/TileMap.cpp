#include "TileMap.h"

TileMap::TileMap(){}

TileMap::~TileMap(){}

TileMap::TileMap(const TileMap& other){}

TileMap& TileMap::operator=(const TileMap& rhs){if (this == &rhs) return *this;return *this;}

/*** Type : Method
**** Used for : View
**** This method is used to load all the elements we need for the "Tilemap"
**** it means this fonction will take 5 args :
**** 1 : link to the file of the tileset
**** 2 : Vector2u of the sizes of the tiles (in our project they are 32x32)
**** 3 : Draw of the level using the tiles by numbers corresponding to their tiles in the tileset
**** 4 : Width of the map (number of tiles)
**** 5 : Height of the map (number of tiles)
***/
bool TileMap::load(const string& tileset, Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height)
{
    // Load the texture from the tileset
    if (!m_tileset.loadFromFile(tileset))
        return false;

    // Fit the table so he can contain all the level
    m_vertices.setPrimitiveType(Quads);
    m_vertices.resize(width * height * 4);

    // Fill the table with one quad/tile
    for (unsigned int i = 0; i < width; ++i)
        for (unsigned int j = 0; j < height; ++j)
        {
            // Get the current tile number
            int tileNumber = tiles[i + j * width];

            // Position in the tileset
            int tu = tileNumber % (m_tileset.getSize().x / tileSize.x);
            int tv = tileNumber / (m_tileset.getSize().x / tileSize.x);

            // Get the pointer of the quad to define in the vertex table
            Vertex* quad = &m_vertices[(i + j * width) * 4];

            // Define his 4 corners
            quad[0].position = Vector2f(i * tileSize.x, j * tileSize.y);
            quad[1].position = Vector2f((i + 1) * tileSize.x, j * tileSize.y);
            quad[2].position = Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
            quad[3].position = Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

            // Define his 4 coords
            quad[0].texCoords = Vector2f(tu * tileSize.x, tv * tileSize.y);
            quad[1].texCoords = Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
            quad[2].texCoords = Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
            quad[3].texCoords = Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
        }

    return true;
}


/*** Type : Method
**** Used for : View
**** This method is used to draw the table
**** it is a redefinition of the draw fonction in sf::drawable
***/
void TileMap::draw(RenderTarget& target, RenderStates states) const
{
    // Apply the transformation
    states.transform *= getTransform();

    // Apply the texture of the tileset
    states.texture = &m_tileset;

    // Draw the vertex table
    target.draw(m_vertices, states);
}

