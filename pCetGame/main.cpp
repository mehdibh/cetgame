#include <iostream>
#include <vector>
#include "Passenger.h"
#include "Bus.h"
#include "TileMap.h"
#include "SpriteCar.h"
#include "RedFire.h"
#include "SpriteBus.h"
#include "SpriteRedFire.h"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include "SpriteCharacter.h"
#include "SpriteBusStop.h"
#include "TextPanel.h"
#include "Menu.h"
#include "GameOverPanel.h"

#include "IGraphicValues.h"

using namespace std;
using namespace sf;

int cameraSpeed = DEFAULT_CAMERASPEED;

int calculerMetre(int a, int b) {
    int pixels = a - b;
    if(pixels < 0) pixels = 0;
    return pixels/40;
}

int main()
{
    // Speed for the VIEW and the BUS (pixels)
    float speedCars = DEFAULT_CAMERASPEED / 2.0f;
    /*************************** Declarations *******************************************/

    SpriteBus bus;
    SpriteRedFire redfire;
    Clock clock;

    //lifePoint
    int lifePoint = 3;

    // humeur

    //count of cars
    int nbcar = 0;
    bool flagcar = true;
    bool isStoppedOnRedfire = false;

    //number of passengers to take
    int nbPassengers = 0;

    //number of movement within two bus stop
    //If this number is too high, it will set the passengers angry
    int nbMovementSinceStop = 0;

    //flag partie en pause
    bool isPaused = false;
    //pointeur d'entier servant � la gestion de plusieurs options
    int* code = new int;

    vector<SpriteCar*> cars;


    TextPanel textPanel;
    textPanel.setLifeScore(lifePoint);
    textPanel.setMood(NORMAL_MOOD);

    Menu menu;

    GameOverPanel gameOver;


    //SpriteCharacter character;
    vector<SpriteCharacter*> characters;
    for(int i = 0; i < 20; i++) {
        SpriteCharacter* charac = new SpriteCharacter();
        characters.push_back(charac->clone());
        delete charac;
    }

    bus.initPosition();


    Clock animateCharactersClock;
    Clock redFireSwap;
    Clock newCarClock;
    Clock refreshBusStop;
    Clock angryPassengerEffect;


    SpriteBusStop* spriteBusStop = new SpriteBusStop();


    bus.initPosition();





    /*************************** SCORE ******************************************/


    /*************************** MAP *******************************************/
    TileMap map;
    if (!map.load("images/tileset.png", Vector2u(32, 32), TILES, 200, 12))
        return -1;

    /*************************** VIEW ******************************************/
    View view(FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT)); // JEU
    View view2(FloatRect(2000, 2000, WINDOW_WIDTH, WINDOW_HEIGHT)); // MENU
    View view3(FloatRect(3000,3000, WINDOW_WIDTH, WINDOW_HEIGHT)); // Game over

    /*************************** Window ****************************************/
    //last argmuents disable window resizibility
    RenderWindow window(VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), GAME_NAME, sf::Style::Titlebar | sf::Style::Close);

    // Variable qui permet de savoir si on doit afficher le menu ou la partie
    int flagPlayGame = 2; // 1 = PARTIE // 2 = MENU // 3 = GAMEOVER

    // MUSIC
    Music music;
    music.openFromFile("music/ridersonthestorm.ogg");
    music.play();
    music.setLoop(true);

    cameraSpeed = 0;

    while (window.isOpen())
    {
        // On regarde si on est dans le menu ou dans la partie
        Event event;

        if(flagPlayGame==2)
        {
            cameraSpeed = 0;
            bus.initPosition();
            window.setView(view2);
            while(window.pollEvent(event))
            {
                // MENU
                if (event.type == sf::Event::MouseButtonPressed)
                {
                    if (event.mouseButton.button == sf::Mouse::Left)
                    {
                        int xMouse = event.mouseButton.x;
                        int yMouse = event.mouseButton.y;
                        if(xMouse>370 && xMouse<670 && yMouse >50 && yMouse<150)
                        {
                            flagPlayGame = 1;
                            cameraSpeed = DEFAULT_CAMERASPEED;
                            bus.takePassengers(-bus.getNbPassengers());
                            view.setCenter(510, view.getCenter().y);
                            bus.initPosition();
                            textPanel.initPosition();
                            redfire.initPosition();
                        }
                        else if(xMouse>370 && xMouse<670 && yMouse >300&& yMouse<400)
                        {
                            return 0;
                        }
                    }
                }
            }
        }
        else if(flagPlayGame==1)
        {
            window.setView(view);
        }
        else if(flagPlayGame==3)
        {
            cameraSpeed = 0;
            bus.initPosition();
            window.setView(view3);
            while(window.pollEvent(event))
            {
                // GAMEOVER
                if (event.type == sf::Event::MouseButtonPressed)
                {
                    if (event.mouseButton.button == sf::Mouse::Left)
                    {
                        int xMouse = event.mouseButton.x;
                        int yMouse = event.mouseButton.y;
                        if(xMouse>370 && xMouse<670 && yMouse >300&& yMouse<400)
                        {
                            flagPlayGame = 2;
                        }
                    }
                }
            }
        }

        // Holding events
        // Event event;
        while (window.pollEvent(event))
        {
            if(event.type == Event::Closed) {
                window.close();
            }
            if(event.type == Event::KeyPressed) {
                // Holding events linked to the keys
                if(event.key.code == Keyboard::Up || event.key.code == Keyboard::Down) {
                    bus.movePosition(event.key.code == Keyboard::Up);
                    nbMovementSinceStop += 1;
                }

                if(event.key.code == Keyboard::Space) {
                    if(spriteBusStop == 0 || cameraSpeed == 0) continue;
                    if(bus.isNextToBusStop(spriteBusStop->getSprite().getPosition().x)) {
                        *code = bus.stopAndFreeze(cameraSpeed, DEFAULT_CAMERASPEED);
                        if(*code != -1) {
                            if(*code == 1) {
                                cameraSpeed = DEFAULT_CAMERASPEED;
                            } else {
                                cameraSpeed = 0;
                                nbPassengers = spriteBusStop->getNbPassengers();
                                bus.takePassengers(nbPassengers);
                                textPanel.increaseScore(nbPassengers);
                                textPanel.setNbPassagersInBus(bus.getNbPassengers());
                                spriteBusStop->resetNbWaitingPassengers();
                                nbPassengers = 0;
                                nbMovementSinceStop = 0;
                                if(bus.isAngry()) { bus.setNormal(); textPanel.setMood(NORMAL_MOOD);}
                                else if (bus.isNormal()) { bus.setHappy();textPanel.setMood(HAPPY_MOOD);}
                                else if (bus.isHappy()) { lifePoint++; if(lifePoint > 3) lifePoint = 3;}
                                clock.restart();
                            }
                        }
                    } else if(bus.isNextToRedFire(redfire.getSprite().getPosition().x)) {
                        if(!redfire.getRedfire()->getIsGreen() &&
                           !(bus.getSprite().getPosition().x-250 >= redfire.getSprite().getPosition().x) ) {
                            cameraSpeed=0;
                            isStoppedOnRedfire = true;
                            clock.restart();
                        }
                    }

                }

                if(event.key.code == Keyboard::Escape) {
                    isPaused = !isPaused;
                    if(isPaused) {
                        cameraSpeed = 0;
                        speedCars = 0;
                    } else {
                        cameraSpeed = DEFAULT_CAMERASPEED;
                        speedCars = DEFAULT_CAMERASPEED / 2.0f;
                    }
                }

            }
        }
        /*if(clock.getElapsedTime().asSeconds() > 1.0f) {
            car.startCours();
            clock.restart();
        }*/
        // Move the car
        /*if(clock.getElapsedTime().asSeconds() > 1.0f) {



            clock.restart();
        }*/
        //Car creation

        //Basic clock
        if(clock.getElapsedTime().asSeconds() > 2.0f) {
            if(!isPaused) {
                if(!isStoppedOnRedfire) {
                    if(cameraSpeed == 0) {
                        cameraSpeed = DEFAULT_CAMERASPEED;
                    }
                }
            }
            clock.restart();
        }
        //Animate Characters
        if(animateCharactersClock.getElapsedTime().asSeconds() > 0.1f) {
            //character.animateWalking();
            for(int i = 0; i < characters.size(); i++) {
                characters[i]->animateWalking();
            }
            animateCharactersClock.restart();
        }
        if(redFireSwap.getElapsedTime().asSeconds() > 8.0f) {
            //swap the color of the redfire every 8 seconds
            redfire.swapColor();
            isStoppedOnRedfire = false;
            redFireSwap.restart();
        }
        if(newCarClock.getElapsedTime().asSeconds() > 4.0f) {
            //create and push in the vector a car every 4 seconds if the vector < 10 cars and the bus do not drive
            if(cars.size()<10&&cameraSpeed!=0){
            cars.push_back(new SpriteCar(bus.getSprite().getPosition().x));
            cars[nbcar]->InitPosition();
            nbcar++;
            }
            newCarClock.restart();


        }
        if(redfire.getSprite().getPosition().x < bus.getSprite().getPosition().x - 100)
        {
            //move the redfire at the next place if the bus is far away
            redfire.movePosition();
        }

        //BUS STOP MANAGEMENT
        if(bus.getSprite().getPosition().x < spriteBusStop->getSprite().getPosition().x) {
            refreshBusStop.restart();
        }
        if(refreshBusStop.getElapsedTime().asSeconds() > 0.1f) {
            if(!spriteBusStop->isDisplayed(bus.getSprite().getPosition().x)) {
                spriteBusStop->refillPassengersAndResetPosition(bus.getSprite().getPosition().x);
            }
            refreshBusStop.restart();
        }

        // move all cars if ...
        for(int i = 0; i < cars.size();i++)
        {
            // if the redfire is green and the bus ins't frontoff
            if(cars[i]->canMove(redfire,bus)||redfire.getRedfire()->getIsGreen())
            {

                for(int j = 0 ; j < cars.size();j++)
                {
                    // if nobody is just fontoff
                    if(cars[i]->getSprite().getPosition().x == cars[j]->getSprite().getPosition().x - 90
                       && j!=i)
                        {
                            flagcar=false;
                            if(cars[i]->getSprite().getPosition().y != cars[j]->getSprite().getPosition().y){
                                flagcar = true;
                            }
                        }
                    // they can move again when the redfire is green
                    if(redfire.getRedfire()->getIsGreen())
                    {
                        if(cars[i]->getSprite().getPosition().x + 85 != cars[j]->getSprite().getPosition().x
                        && j!=i)
                        {
                            flagcar = true;
                        }
                    }
                }
                // everything it's ok so the flagcar is true, the car can move
                if(flagcar){
                    cars[i]->startCours(speedCars);
                }
                flagcar = true;
            }

            if(cars[i]->destroyVehicule(bus.getSprite().getPosition().x,bus.getSprite().getPosition().y, code))
            {
                delete cars[i];
                cars.erase(cars.begin()+i);
                nbcar--;
                if(*code != 1) { textPanel.increaseScore(1); } else {lifePoint--;}
            }
        }

        //Detect redfire outpassement
        if(bus.getSprite().getPosition().x+50 >= redfire.getSprite().getPosition().x && bus.getSprite().getPosition().x+50 <= redfire.getSprite().getPosition().x+50) {
            if(!redfire.getRedfire()->getIsGreen()) {
                lifePoint--;
                textPanel.setLifeScore(lifePoint);
                redfire.swapColor();
                redFireSwap.restart();
            }
        }

        //Set the effect of angryness on the score
        if(angryPassengerEffect.getElapsedTime().asSeconds() > 3.0f) {
            if(nbMovementSinceStop >= 20 && !bus.isAngry()) {
                bus.setAngry();
                textPanel.setMood(ANGRY_MOOD);
            }
            if(bus.isAngry()) {
                textPanel.decreaseScore(1);
            }
            angryPassengerEffect.restart();
        }


        // Same speed for the view and the bus
        view.move(cameraSpeed, 0);

        bus.followView(cameraSpeed);

        textPanel.followView(cameraSpeed);

        /***************************************************/
        /**************** RESET OF THE MAP *****************/
        /***************************************************/
        if(view.getCenter().x >= 5888) {
            view.setCenter(510, view.getCenter().y);
            // ROLL BACK BUS
            bus.resetPositionX();

            // ROLL BACK CARS IN FRONT OF THE BUS
            for(int i = 0; i < cars.size();i++)
            {
                cars[i]->setRollBackCar(5378);
            }
            redfire.initPosition();
            textPanel.initPosition();
            spriteBusStop->setRollback(5378);
        }

        /***************************************************/
        for(int i = 0; i < characters.size(); i++) {
            characters[i]->walk(cameraSpeed / 2.0f);
        }
        //sprite.move(property->getCameraSpeed() / 3, 0);
        // draw the map
        window.clear();
        window.draw(map);
        // draw the sprites
        for(int i = 0; i < cars.size();i++)
        {
            window.draw(cars[i]->getSprite());
        }
        window.draw(redfire.getSprite());
        window.draw(bus.getSprite());
        if(spriteBusStop != 0) { window.draw(spriteBusStop->getSprite()); }

        // TextPanel components
        textPanel.setLifeScore(lifePoint);
        textPanel.setProchainArret(calculerMetre(spriteBusStop->getSprite().getPosition().x, bus.getSprite().getPosition().x));
        window.draw(textPanel.getScoreText());
        window.draw(textPanel.getNbPassagersInBusText());
        window.draw(textPanel.getProchainArretText());
        window.draw(textPanel.getLifeScoreText());
        window.draw(textPanel.getMoodText());

        // MENU
        Vector2i globalPosition = Mouse::getPosition(window);
        window.draw(menu.getBackgroundMenu());
        window.draw(menu.getSpriteToPrintJouer(globalPosition));
        window.draw(menu.getSpriteToPrintQuitter(globalPosition));

        // Game Over
        window.draw(gameOver.getSpriteBackground());
        window.draw(gameOver.getSpriteToPrintQuitter(globalPosition));

        //window.draw(menu.getSpriteJouer());
        /*
        for(int i = 0; i < characters.size(); i++) {
            window.draw(characters[i]->getSprite());
        }*/
        sleep(milliseconds(15));

        // Resetting all the values for the next game
        if(lifePoint<=0){
            flagPlayGame = 3;
            lifePoint = 3;
            nbPassengers = 0;
            textPanel.setScore(0);
            textPanel.setNbPassagersInBus(nbPassengers);
            textPanel.setLifeScore(lifePoint);
            nbMovementSinceStop =0;
            bus.setNormal();
            textPanel.setMood(NORMAL_MOOD);
        }

        window.display();
    }
    for(int i = 0; i < characters.size(); i++) {
        delete characters[i];
    }
    for(int i = 0; i < cars.size(); i++) {
        delete cars[i];
    }

    delete spriteBusStop;
    delete code;
    return 0;
}
